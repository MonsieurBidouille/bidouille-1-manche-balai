# Bidouille 1 - Manche à balai

Documentation du bateau en manches à balai. Vidéo ici : https://youtu.be/zdK9L4kbwqo

Je ne peux plus partager le lien public Fusion 360, c'est une fonction payante (je ne savais pas avec la version d'évaluation qui a expirée)

Les fichiers sont donc des exports que j'ai placés dans /3D - Fusion360

Jonctions faites pour des manches métal de 22mm de diamètre.

Vis de 6mm fois 30mm de long tête hexagonale. 

Je vous invite à concevoir vos propres jonctions par rapport aux manches à balais que vous avez. 